/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package webviewfx;

import java.io.IOException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author bluesprogrammer
 */
public class WebViewFX extends Application {
    
    @Override
    public void start(Stage primaryStage) throws Exception{
        
        StackPane root = new StackPane();
        
        WebView view = new WebView();
        WebEngine engine = view.getEngine();
        engine.load("http://javapiteresina.blogspot.com.br/");
        root.getChildren().add(view);
        
        Scene scene = new Scene(root, 900, 700);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
        
    public static void main(String[] args) throws IOException{
        Application.launch(args);
    }
    
}
